using GSRepository;
using GWBusiness;
using GWRepository;
using GWServices;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace GWServicesTest
{
    internal class FakeRepository : IRepository<Subscription>
    {
        public void Delete(Subscription entity)
        {
            throw new NotImplementedException();
        }

        public Subscription Get(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Subscription> GetAll()
        {
            return DataGenerator.SetupCompleteSubscriptionOverview();
        }

        public void Insert(Subscription entity)
        {
            throw new NotImplementedException();
        }

        public void Remove(Subscription entity)
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }

        public void Update(Subscription entity)
        {
            throw new NotImplementedException();
        }
    }

    public class TestSubscriptionService
    {
        [Fact]
        public void GetSubscription()
        {
            // arrange
            var fakeRepository = new FakeRepository();
            var service = new SubscriptionService(fakeRepository, null);

            // act
            var subscriptions = service.GetAllSubscriptions();

            // assert
            Assert.True(subscriptions.Count() == 3);
        }
    }
}
