﻿using System;
using System.Collections.Generic;
using System.Text;
using GSRepository;
using GWBusiness;
using Microsoft.EntityFrameworkCore;
namespace GWRepository
{
    public class GWContext : DbContext
    {
        public GWContext(DbContextOptions<GWContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            new SubscriptionMap(modelBuilder.Entity<Subscription>());
            new VehicleTypeMap(modelBuilder.Entity<VehicleType>());
        }
    }
}
