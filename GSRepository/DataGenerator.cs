﻿using GWBusiness;
using GWRepository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GSRepository
{
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using var context = new GWContext(
                serviceProvider.GetRequiredService<DbContextOptions<GWContext>>());
            if (context.Set<Subscription>().Any())
            {
                return;
            }

            var occasional = SetupOccasional();
            var regular = SetupRegular();
            var frequent = SetupFrequent();

            context.Set<Subscription>().AddRange(
            occasional, regular, frequent);
            context.SaveChanges();
        }

        public static IEnumerable<Subscription> SetupCompleteSubscriptionOverview()
        {
            var subscriptions = new List<Subscription>
            {
                SetupOccasional(), SetupRegular(), SetupFrequent()
            };

            return subscriptions;
        }

        private static Subscription SetupOccasional()
        {
            return new Subscription
            {
                Id = Guid.NewGuid(),
                Name = "Occasional",
                VehicleTypes = new List<VehicleType>
                    {
                        new VehicleType
                        {
                            Id = Guid.NewGuid(),
                            Name = "Small",
                            PricePerHour = new HourPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = 6.00M,
                                Currency = Currency.Euro
                            },
                            PricePerKilometer = new KilometerPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = .34M,
                                Currency = Currency.Euro
                            }
                        },
                        new VehicleType
                        {
                            Id = Guid.NewGuid(),
                            Name = "Large",
                            PricePerHour = new HourPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = 7.50M,
                                Currency = Currency.Euro
                            },
                            PricePerKilometer = new KilometerPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = .39M,
                                Currency = Currency.Euro
                            }
                        },
                        new VehicleType
                        {
                            Id = Guid.NewGuid(),
                            Name = "Electric",
                            PricePerHour = new HourPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = 11.00M,
                                Currency = Currency.Euro
                            },
                            PricePerKilometer = new KilometerPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = .20M,
                                Currency = Currency.Euro
                            }
                        }
                    }
            };
        }

        private static Subscription SetupRegular()
        {
            return new Subscription
            {
                Id = Guid.NewGuid(),
                Name = "Regular",
                VehicleTypes = new List<VehicleType>
                    {
                        new VehicleType
                        {
                            Id = Guid.NewGuid(),
                            Name = "Small",
                            PricePerHour = new HourPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = 4.00M,
                                Currency = Currency.Euro
                            },
                            PricePerKilometer = new KilometerPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = .29M,
                                Currency = Currency.Euro
                            }
                        },
                        new VehicleType
                        {
                            Id = Guid.NewGuid(),
                            Name = "Large",
                            PricePerHour = new HourPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = 5.50M,
                                Currency = Currency.Euro
                            },
                            PricePerKilometer = new KilometerPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = .34M,
                                Currency = Currency.Euro
                            }
                        },
                        new VehicleType
                        {
                            Id = Guid.NewGuid(),
                            Name = "Electric",
                            PricePerHour = new HourPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = 9.00M,
                                Currency = Currency.Euro
                            },
                            PricePerKilometer = new KilometerPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = .15M,
                                Currency = Currency.Euro
                            }
                        }
                    }
            };
        }

        private static Subscription SetupFrequent()
        {
            return new Subscription
            {
                Id = Guid.NewGuid(),
                Name = "Frequent",
                VehicleTypes = new List<VehicleType>
                    {
                        new VehicleType
                        {
                            Id = Guid.NewGuid(),
                            Name = "Small",
                            PricePerHour = new HourPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = 3.00M,
                                Currency = Currency.Euro
                            },
                            PricePerKilometer = new KilometerPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = .24M,
                                Currency = Currency.Euro
                            }
                        },
                        new VehicleType
                        {
                            Id = Guid.NewGuid(),
                            Name = "Large",
                            PricePerHour = new HourPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = 4.50M,
                                Currency = Currency.Euro
                            },
                            PricePerKilometer = new KilometerPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = .29M,
                                Currency = Currency.Euro
                            }
                        },
                        new VehicleType
                        {
                            Id = Guid.NewGuid(),
                            Name = "Electric",
                            PricePerHour = new HourPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = 8.00M,
                                Currency = Currency.Euro
                            },
                            PricePerKilometer = new KilometerPrice
                            {
                                Id = Guid.NewGuid(),
                                Amount = .12M,
                                Currency = Currency.Euro
                            }
                        }
                    }
            };
        }
    }
}
