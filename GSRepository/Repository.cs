﻿using GWBusiness;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GWRepository
{
    public interface IRepository<SubScription>
    {
        IEnumerable<Subscription> GetAll();
        SubScription Get(long id);
        void Insert(Subscription entity);
        void Update(Subscription entity);
        void Delete(Subscription entity);
        void Remove(Subscription entity);
        void SaveChanges();

    }

    public class Repository : IRepository<Subscription>
    {
        private readonly GWContext context;
        private readonly DbSet<Subscription> subscriptions;

        public Repository(GWContext context)
        {
            this.context = context;
            this.subscriptions = this.context.Set<Subscription>();
        }

        public void Delete(Subscription entity)
        {
            throw new NotImplementedException();
        }

        public Subscription Get(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Subscription> GetAll()
        {
            return this.subscriptions
                .Include(n => n.VehicleTypes)
                .ThenInclude(n => n.PricePerKilometer)
                .Include(n => n.VehicleTypes)
                .ThenInclude(n => n.PricePerHour)
                .AsEnumerable();
        }

        public void Insert(Subscription entity)
        {
            throw new NotImplementedException();
        }

        public void Remove(Subscription entity)
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }

        public void Update(Subscription entity)
        {
            throw new NotImplementedException();
        }
    }
}
