﻿using GWBusiness;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GSRepository
{
    internal class SubscriptionMap
    {
        public SubscriptionMap(EntityTypeBuilder<Subscription> entityBuilder)
        {
            entityBuilder.HasKey(n => n.Id);
            entityBuilder.HasMany(n => n.VehicleTypes);
        }
    }
}
