﻿using GWBusiness;
using GWRepository;
using System.Collections.Generic;

namespace GWServices
{
    public interface ISubscriptionService
    {
        IEnumerable<Subscription> GetAllSubscriptions();
        decimal CalculateFeeForSubscription(ActualSubscription subscription, int numberOfKilometers, int numberOfHours);
    }

    public class SubscriptionService : ISubscriptionService
    {
        private readonly IRepository<Subscription> subscriptionRepository;
        private readonly ISubscriptionPriceCalculator priceCalculator;

        public SubscriptionService(IRepository<Subscription> subscriptionRepository, 
            ISubscriptionPriceCalculator priceCalculator)
        {
            this.subscriptionRepository = subscriptionRepository;
            this.priceCalculator = priceCalculator;
        }
        public IEnumerable<Subscription> GetAllSubscriptions()
        {
            return subscriptionRepository.GetAll();
        }

        public decimal CalculateFeeForSubscription(ActualSubscription subscription, int numerOfKilometers, int numberOfHours)
        {
            return priceCalculator.CalculatePrice(subscription.VehicleType, numerOfKilometers, numberOfHours);
        }
    }
}
