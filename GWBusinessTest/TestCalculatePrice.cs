using GWBusiness;
using System;
using Xunit;

namespace GWBusinessTest
{
    public class TestCalculatePrice
    {
        [Fact]
        public void CalculatePrice()
        {
            // arrange
            var calculator = new PriceCalculator();

            var chosenSubScription = new ActualSubscription
            {
                Name = "Occasional",
                VehicleType = new VehicleType
                {
                    Id = Guid.NewGuid(),
                    Name = "Small",
                    PricePerHour = new HourPrice
                    {
                        Id = Guid.NewGuid(),
                        Amount = 6,
                        Currency = Currency.Euro
                    },
                    PricePerKilometer = new KilometerPrice
                    {
                        Id = Guid.NewGuid(),
                        Currency = Currency.Euro,
                        Amount = .34M
                    }
                }
            };

            var numberOfKilometers = 100;
            var numberOfHours = 2;
            var expectedPrice = 46M;

            // act
            var actualPrice = calculator.CalculatePrice(chosenSubScription.VehicleType, numberOfKilometers, numberOfHours);


            // assert
            Assert.Equal(expectedPrice, actualPrice);
        }
    }
}
