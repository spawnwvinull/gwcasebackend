﻿using GWBusiness;
using GWCaseBackend.ViewModels;
using GWServices;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GWCaseBackend.Controllers
{
    [EnableCors]
    [ApiController]
    [Route("[controller]")]
    public class SubscriptionController : ControllerBase
    {
        private readonly ISubscriptionService subscriptionService;

        public SubscriptionController(ISubscriptionService subscriptionService)
        {
            this.subscriptionService = subscriptionService;
        }

        [HttpGet]
        public Subscriptions Get()
        {
            var subscriptions = this.subscriptionService.GetAllSubscriptions();
            return new Subscriptions
            {
                SubscriptionItems = subscriptions
            };
        }

        [HttpPost]
        public decimal Calculate([FromBody]ActualSubscriptionViewModel vm)
        {
            var actualSubscription = this.GetActualSubscription(vm.SubscriptionName, vm.VehicleTypeName);
            return this.subscriptionService.CalculateFeeForSubscription(actualSubscription, vm.NumberOfKilometers, vm.NumberOfHours);
        }

        private ActualSubscription GetActualSubscription(string subscriptionName, string vehicleTypeName)
        {
            var subscriptions = this.subscriptionService.GetAllSubscriptions();
            var currentSubscription = subscriptions.SingleOrDefault(n => n.Name == subscriptionName);
            var currentVehicleType = currentSubscription.VehicleTypes.SingleOrDefault(n => n.Name == vehicleTypeName);
            var actualSubscription = new ActualSubscription
            {
                Name = currentSubscription.Name,
                VehicleType = currentVehicleType
            };

            return actualSubscription;
        }
    }
}
