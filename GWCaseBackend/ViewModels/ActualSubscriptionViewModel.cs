﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GWCaseBackend.ViewModels
{
    public class ActualSubscriptionViewModel
    {
        public string SubscriptionName { get; set; }
        public string VehicleTypeName { get; set; }

        public int NumberOfHours { get; set; }
        public int NumberOfKilometers { get; set; }
    }
}
