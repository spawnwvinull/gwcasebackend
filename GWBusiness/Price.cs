﻿using System;

namespace GWBusiness
{
    public class Price : BaseEntity
    {
        public Currency Currency { get; set; }

        public Decimal Amount { get; set; }
    }

    public class HourPrice : Price { }

    public class KilometerPrice : Price { }
}
