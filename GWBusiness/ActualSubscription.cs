﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GWBusiness
{
    public class ActualSubscription
    {
        public string Name { get; set; }

        public VehicleType VehicleType { get; set; }
    }
}
