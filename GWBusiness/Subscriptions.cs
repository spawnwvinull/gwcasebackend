﻿using System.Collections.Generic;

namespace GWBusiness
{
    public class Subscriptions
    {
        public IEnumerable<Subscription> SubscriptionItems { get; set; }
    }
}
