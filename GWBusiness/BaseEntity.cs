﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GWBusiness
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}
