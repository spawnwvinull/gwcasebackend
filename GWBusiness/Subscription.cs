﻿using System.Collections.Generic;

namespace GWBusiness
{
    public class Subscription : BaseEntity
    {
        public string Name { get; set; }

        public List<VehicleType> VehicleTypes { get; set; }
    }
}
