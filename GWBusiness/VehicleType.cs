﻿
namespace GWBusiness
{
    public class VehicleType : BaseEntity
    {
        public string Name { get; set; }

        public HourPrice PricePerHour { get; set; }

        public KilometerPrice PricePerKilometer { get; set; }
    }
}
