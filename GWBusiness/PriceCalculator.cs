﻿using System;

namespace GWBusiness
{
    public interface ISubscriptionPriceCalculator
    {
        decimal CalculatePrice(VehicleType vehicleType, int numberOfKilomters, int numberOfHours);
    }


    public class PriceCalculator : ISubscriptionPriceCalculator
    {
        public Decimal CalculatePrice(VehicleType vehicleType, int numberOfkilometers, int numberOfHours)
        {
            return vehicleType.PricePerHour.Amount * numberOfHours + vehicleType.PricePerKilometer.Amount * numberOfkilometers;
        }
    }
}
